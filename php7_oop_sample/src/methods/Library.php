<?php

namespace Grzegab\LibraryExample\methods;

use Grzegab\LibraryExample\ppl;

class Library
{
    /**
     * Define constants for status
     */
    private const LIBRARY_IS_OK = true;
    private const LIBRARY_IS_NOT_OK = false;

    /**
     * Get librarys status, is it need to be closed
     * @return bool
     */
    public static function isLibraryOk(): bool
    {
        $pplCount = \count($_SESSION['ppl']) ?? 0;
        $mediaCount = \count($_SESSION['media']) ?? 0;
        if ($pplCount > 10 && $mediaCount > 2) {
            return self::LIBRARY_IS_OK;
        }
        return self::LIBRARY_IS_NOT_OK;

    }

    /**
     * Clean library by one of staff members
     */
    public function clean(): void
    {
        $isClean = $_SESSION['library']['clean'];
        if(!$isClean) {
            foreach ($_SESSION['ppl'] as $person) {
                if($person instanceof ppl\Staff) {
                    echo sprintf('<p>%s has cleaned library</p>', $person->getName());
                    break;
                }
            }
        } else {
            echo "<p>Library already clean</p>";
        }

    }


}