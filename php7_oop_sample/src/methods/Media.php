<?php
namespace Grzegab\LibraryExample\methods;

use Grzegab\LibraryExample\media\{Book, Cd, Movie};

class Media {

    /**
     * Save data to session
     */
    public function save(): void
    {
        if (empty($_REQUEST['type'])) {
            throw new \Exception('No such media type');
        }

        if($_REQUEST['type'] === 'book') {
            $_SESSION['media'][] = new Book($_REQUEST['name'], $_REQUEST['pages']);
        } elseif($_REQUEST['type'] === 'movie') {
            $_SESSION['media'][] = new Movie($_REQUEST['name'], $_REQUEST['director'], $_REQUEST['runtime']);
        } elseif($_REQUEST['type'] === 'cd') {
            $_SESSION['media'][] = new Cd($_REQUEST['name'], $_REQUEST['artist'], $_REQUEST['songs']);
        }
    }

    /**
     * Add new media to session
     */
    public function add(): void
    {
        echo "<h1>Add Media</h1>";
        echo '<form action="index.php" method="post">';
        echo "Title: <input name='name' />";
        echo "Pages: <input name='pages' />";
        echo "Director: <input name='director' />";
        echo "Runtime: <input name='runtime' />";
        echo "Artist: <input name='artist' />";
        echo "<input type='hidden' name='method' value='Media' />";
        echo "<input type='hidden' name='action' value='save' />";
        echo "<select name=\"type\">";
        echo "<option value=\"book\">Book</option>";
        echo "<option value=\"movie\">Movie</option>";
        echo "<option value=\"cd\">CD</option>";
        echo "</select>";
        echo "<button type='submit'>Submit</button>";
        echo '</form>';
    }

    /**
     * Remove existing media, return true if success
     * @return bool
     */
    public function remove(): bool
    {

        if (!empty($_REQUEST['id'])) {
            foreach ($_SESSION['media'] as $index => $media) {
                if ($media->getId() === (int)$_REQUEST['id']) {
                    unset($_SESSION['media'][$index]);
                    break;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Get media list with remove option and a list of rented media
     */
    public function list(): void
    {
        echo "<h1>Media list</h1>";
        foreach ($_SESSION['media'] as $media) {
            if ($media instanceof Movie) {
                echo sprintf(
                    "<p>Movie: %s, Director: %s, Duration: %s</p>",
                    $media->getTitle(), $media->getDirector(), $media->getRunTime()
                );
            } elseif ($media instanceof Book) {
                echo sprintf(
                    "<p>Book: %s, Pages: %s</p>",
                    $media->getTitle(), $media->getPages()
                );
            } elseif ($media instanceof Cd) {
                echo sprintf(
                    "<p>CD: %s, Artist: %s</p>",
                    $media->getTitle(), $media->getArtist()
                );
                echo "<ul>";
                foreach ($media->getSongs() as $song) {
                    echo sprintf('<li>%s</li>', $song);
                }
                echo "</ul>";
            } else {
                throw new \Exception('unknown media type');
            }
            echo sprintf(
                "<a href='index.php?method=Media&action=remove&id=%d'>Remove</a>",
                $media->getId()
            );

        }
    }
}

