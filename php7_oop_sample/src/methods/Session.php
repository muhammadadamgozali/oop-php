<?php
/**
 * Session file
 */

namespace Grzegab\LibraryExample\methods;

use Grzegab\LibraryExample\ppl as ppl;
use Grzegab\LibraryExample\media as media;
use Grzegab\LibraryExample\methods as method;

class Session
{
    /**
     * Added some dummy content to start with
     *  - Let's say library was just build in a small town
     */
    private function addDummyData()
    {
        $_SESSION['ppl']['masterLibrarian'] = new ppl\Librarian('Matt K', 45, true);
        $_SESSION['ppl']['librarian'] = new ppl\Librarian('Verionica K', 34, true);
        $_SESSION['ppl']['juniorLibrarian'] = new ppl\Librarian('Vincent U', 22);
        $_SESSION['ppl']['staffMember'] = new ppl\Librarian('Eric W', 67);

        $_SESSION['ppl']['student1'] = new ppl\Student('Joe T', 19);
        $_SESSION['ppl']['student2'] = new ppl\Student('Chandler B', 17);
        $_SESSION['ppl']['student3'] = new ppl\Student('Monica G', 20);
        $_SESSION['ppl']['student4'] = new ppl\Student('Ross G', 21);
        $_SESSION['ppl']['student5'] = new ppl\Student('Rachel G', 17);

        $_SESSION['ppl']['client1'] = new ppl\Client('Tomm H', 54);
        $_SESSION['ppl']['client2'] = new ppl\Client('Christina A', 54);

        $_SESSION['media']['book1'] = new media\Book('Forest Gump', 254);
        $_SESSION['media']['book2'] = new media\Book('Glasgow', 24);

        $_SESSION['media']['movie1'] = new media\Movie('Die Hard', 'Tom Hanks', 221);
        $_SESSION['media']['movie2'] = new media\Movie('Die Hard 2', 'Tom Hanks', 187);

        $_SESSION['media']['cd1'] = new media\Cd('Preludium', 'Tool', ['Track1', 'Track2', 'Track3']);
        $_SESSION['media']['cd2'] = new media\Cd('Eminem back', 'Eminem', ['Track1', 'Track2', 'Track3']);

        $_SESSION['library']['clean'] = false;

    }

    public function start($debug = false)
    {
        session_save_path ('./tmp');
        session_start();

        if(empty(session_id())) {
            if($debug) {
                ini_set('display_errors', 'on'); error_reporting(-1);
                echo 'session now started: '.session_id();
                $this->addDummyData();
            }
        } else {
            if($debug) {
                echo 'session already started: ' . session_id();
            }

        }
    }

    public function status($show = false)
    {
        if($show) {
            if ( !is_writable(session_save_path()) ) {
                echo 'Session save path "'.session_save_path().'" is not writable!';
            } else {
                echo 'Session save path "'.session_save_path().'" is writable!';
            }
            var_dump($_SESSION);
        }
        return false;
    }
}