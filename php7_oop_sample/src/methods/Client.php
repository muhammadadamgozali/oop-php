<?php

namespace Grzegab\LibraryExample\methods;

use Grzegab\LibraryExample\ppl;

class Client
{

    /**
     * Save data to session
     */
    public function save(): void
    {
        if (!empty($_REQUEST['type']) && $_REQUEST['type'] === 'student') {
            $_SESSION['ppl'][] = new ppl\Student($_REQUEST['name'], $_REQUEST['age']);
        } else {
            $_SESSION['ppl'][] = new ppl\Client($_REQUEST['name'], $_REQUEST['age']);
        }
    }

    /**
     * Remove existing client, return true if success
     * @return bool
     */
    public function remove(): bool
    {

        if (!empty($_REQUEST['id'])) {

            foreach ($_SESSION['ppl'] as $index => $person) {
                if ($person->getId() === (int)$_REQUEST['id']) {
                    unset($_SESSION['ppl'][$index]);
                    break;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Add new client to session
     */
    public function add(): void
    {
        echo "<h1>Add Client</h1>";
        echo '<form action="index.php" method="post">';
        echo "Name: <input name='name' />";
        echo "Age: <input name='age' />";
        echo "<input type='hidden' name='method' value='Client' />";
        echo "<input type='hidden' name='action' value='save' />";
        echo "<select name=\"type\">";
        echo "<option value=\"\">Regular</option>";
        echo "<option value=\"student\">Student</option>";
        echo "</select>";
        echo "<button type='submit'>Submit</button>";
        echo '</form>';
    }

    /**
     * Get client list with remove option and a list of rented media
     */
    public function list(): void
    {
        echo "<h1>Client list</h1>";
        foreach ($_SESSION['ppl'] as $person) {
            if ($person instanceof ppl\Client) {
                $clientType = 'Regural client';
                if ($person instanceof ppl\Student) {
                    $clientType = 'Student';
                }
                echo sprintf(
                    "<p>Client type: %s, Name: %s, Age: %s</p>",
                    $clientType, $person->getName(), $person->getAge()
                );
                echo '<p>Have rented: <br/>';
                if (empty($person->getRented())) {
                    echo 'None';
                } else {
                    echo "<ul>";
                    foreach ($person->getRented() as $rented) {
                        echo sprintf('<li>%s</li>', $rented);
                    }
                    echo "</ul>";
                }
                echo '</p>';
                echo sprintf(
                    "<a href='index.php?method=Client&action=remove&id=%d'>Remove</a>",
                    $person->getId()
                );
            }
        }
    }

}

