<?php
namespace Grzegab\LibraryExample\media;

class Book extends Media
{
    /**
     * Books have additional pages count
     * @var int
     */
    private $pages;

    /**
     * Book constructor.
     * @param string $title
     * @param int $pages
     * @param bool $isCheckOut
     */
    public function __construct(string $title, int $pages, bool $isCheckOut = false)
    {
        parent::__construct($title, $isCheckOut);
        $this->pages = $pages;
    }

    /**
     * Return pages count
     * @return int
     */
    public function getPages(): int
    {
        return $this->pages;
    }
}