<?php

namespace Grzegab\LibraryExample\media;

class Media
{
    private static $allMedia = 0;

    private $id;
    private $title;
    private $isCheckOut;
    private $ratings = [];

    public function __construct(string $title, bool $isCheckOut = false)
    {
        $this->title = $title;
        $this->isCheckOut = $isCheckOut;

        $this->id = 1 + self::$allMedia++;
    }

    public static function getMediaCount(): int
    {
        return self::$allMedia;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function addRating(float $rating)
    {
        $ratings[] = $rating;
    }

    public function getRating(): float
    {
        return array_sum($this->ratings) / \count($this->ratings);
    }
}