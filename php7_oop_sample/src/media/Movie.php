<?php

namespace Grzegab\LibraryExample\media;

class Movie extends Media
{
    /**
     * Moves have additional runtime and director information
     * @var
     */
    private $runTime;
    private $director;

    /**
     * Movie constructor.
     * @param string $title
     * @param string $director
     * @param int $runtime
     * @param bool $isCheckOut
     */
    public function __construct(string $title, string $director, int $runtime, bool $isCheckOut = false)
    {
        parent::__construct($title, $isCheckOut);
        $this->director = $director;
        $this->runTime = $runtime;
    }

    /**
     * @return mixed
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * @return mixed
     */
    public function getRunTime()
    {
        return $this->runTime;
    }

}