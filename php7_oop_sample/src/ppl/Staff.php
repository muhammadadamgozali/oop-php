<?php

namespace Grzegab\LibraryExample\ppl;
/**
 * Basic worker for library
 */
class Staff extends Person
{
    public function __construct($name, $age, $isAdmin = false)
    {
        parent::__construct($name, $age, $isAdmin);
    }
}