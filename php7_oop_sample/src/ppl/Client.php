<?php

namespace Grzegab\LibraryExample\ppl;

use Grzegab\LibraryExample\media\Media;

/**
 * Each person can rent media
 */

class Client extends Person
{
    /**
     * Array for all rented media
     *
     * @var array
     */
    private $rented = [];

    /**
     * Client constructor.
     * @param string $name
     * @param int $age
     * @param bool $isAdmin
     */
    public function __construct($name, $age, $isAdmin = false)
    {
        parent::__construct($name, $age, $isAdmin);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getRented(): array
    {
        if (!empty($_REQUEST['id'])) {
            foreach ($_SESSION['ppl'] as $index => $sessionPerson) {
                if ((int)$_REQUEST['id'] === $sessionPerson->getId()) {
                    $toReturn = $_SESSION['ppl'][$index]['rented'];
                }
            }
            return $toReturn;
        } else {
            throw new \Exception('No such client.');
        }

    }
}