<?php

namespace Grzegab\LibraryExample\ppl;

/**
 * Default class for person, defines a human using library
 *
 * Class Person
 */
class Person
{
    private static $pplUsingLibrary = 0; //only ppl who want to rent some media
    private static $allPpl = 0; //all ppl with workers

    private $id;
    private $name;
    private $age;
    private $isAdmin;


    public function __construct(string $name, int $age, $isAdmin = false)
    {
        $this->name = $name;
        $this->age = $age;
        $this->isAdmin = $isAdmin;

        if (!$isAdmin) {
            self::$pplUsingLibrary++;
        }

        $this->id = ++self::$allPpl;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAge(): int
    {
        $toReturn = $this->age;
        if ($this->isAdmin) {
            $toReturn = 100; //We want admins to look experienced ;)
        }
        return (int)$toReturn;
    }

    /**
     * We want to get only client number to see if library is needed
     *
     * @return mixed
     */
    final public static function pplUsingLibrary()
    {
        return self::$pplUsingLibrary;
    }

    /**
     * We want to get number of all ppl connected to library, workers and clients
     *
     * @return mixed
     */
    final public static function allPplUsingLibrary()
    {
        return self::$allPpl;
    }

}