A sample PHP 7.x project
---
It's a sample project to show OOP only. 
It uses session and no auth components.
It doesn't use MVC pattern nor routing,
 it has to be as simple as it can.
~~~
There are two main classes: Person and Media.
~~~

To install run `composer install`.

To run simply `open main dir in browser`.

Things used here:
1. PHP (tested on 7.1 version and will work with 7.x versions)
2. Composer with:
    * Autoloader (PSR-4)
    
