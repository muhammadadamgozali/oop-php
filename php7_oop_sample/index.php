<?php
/**
 * Debug, if true debug info will be show
 */

const DEBUG_STATUS = true;

/**
 * Load autoloader and use files
 */
require_once __DIR__ . '/vendor/autoload.php';

/**
 * Load necessary methods to control library
 */
use Grzegab\LibraryExample\methods as method;
use Grzegab\LibraryExample\ppl as person;
use Grzegab\LibraryExample\media;

/**
 * Bootstrap file to start with
 */
$session = new method\Session();
$session->start(DEBUG_STATUS);

/**
 * Show available options as links to other
 * This should go with get and post params
 * In other approach we could add separate files for methods but each should contain autoload
 */
$allowedMethods = [
    'Client' => ['list', 'add'],
    'Media' => ['list', 'add'],
    'Library' => 'clean'
];

/**
 * Basic routing setting - it doesn't verify if path exists or is correct,
 * only something basic to make all this work
 */
if (!empty($_REQUEST['method'])) {
    //select class to use
    if($_REQUEST['method'] === 'Client') {
        $class = new method\Client();
    } elseif ($_REQUEST['method'] === 'Media') {
        $class = new method\Media();
    } elseif($_REQUEST['method'] === 'Library'){
        $class = new method\Library();
    } else {
        throw new ErrorException('Class not found.');
    }

    //select method
    if(!empty($_REQUEST['action'])) {
        if($_REQUEST['action'] === 'add') {
            $class->add();
        } elseif ($_REQUEST['action'] === 'remove') {
            $class->remove();
        } elseif ($_REQUEST['action'] === 'list') {
            $class->list();
        } elseif ($_REQUEST['action'] === 'rent') {
            $class->rent();
        } elseif ($_REQUEST['action'] === 'return') {
            $class->return();
        } elseif ($_REQUEST['action'] === 'clean') {
            $class->clean();
        } elseif ($_REQUEST['action'] === 'save') {
            $class->save();
        } else {
            throw new ErrorException('Method not found.');
        }

    }
}

/**
 * List available actions
 */
echo "<h2>Options</h2>";
echo "<p><a href='index.php'>Index page</a></p>";
foreach ($allowedMethods as $k => $v) {
    if(is_array($v)) {
        foreach($v as $item) {
            echo sprintf("<p><a href='index.php?method=%s&action=%s'>%s - %s</a></p>",  $k, $item, $k, $item);
        }
    } else {
        echo sprintf("<p><a href='index.php?method=%s&action=%s'>%s - %s</a></p>", $k, $v, $k, $v);
    }

}

/**
 * Show current status about ppl
 */
echo "<h2>Info</h2>";
$allPpl = person\Person::allPplUsingLibrary();
$clients = person\Person::pplUsingLibrary();
$workers = $allPpl - $clients;
echo sprintf("<p>We have %d people (that contains %d admin workers)</p>", $allPpl, $workers);


/**
 * Show current status about media
 */
echo sprintf("<p>We have %d Media in library</p>", media\Media::getMediaCount());

/**
 * Is library prospering well
 */
if(method\Library::isLibraryOk()) {
    echo "<p>Library is just fine.</p>";
} else {
    echo "<p>Library need help! Not enough media or people!</p>";
}
/**
 * Debug information about session, available only if debug status if set to true
 */
$session->status(DEBUG_STATUS);